//server variables for initialization
const express = require('express'); 
const mongoose = require('mongoose'); 
require('dotenv').config()
const app = express();
const port = 4000;

//MongoDB Connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@batch303-exclamador.g2jjkdn.mongodb.net/b303-todo?retryWrites=true&w=majority`,{
	useNewUrlParser: true,
	useUnifiedTopology: true

});

let database = mongoose.connection;

database.on('error', () => console.log('Connection error :('));
database.once('open', () => console.log('Connected to MongoDB!'));

//Middleware Connection
app.use(express.json()); 
app.use(express.urlencoded({extended: true}));

//[SECTION] Mongoose Schema
const taskSchema = new mongoose.Schema({
	name:String,
	status:{
		type: String,
	default: "pending"
	}
});

//[SECTION] Modules
const Task = mongoose.model("Task" ,taskSchema);

//[SECTION] Routes
//creating a new task
app.post('/tasks', (request,response) =>{
	Task.findOne({name: request.body.name}).then((result,error) =>{
		if(result != null && result.name == request.boby.name){
			return response.send("Duplicate task found!");
		}else {
			let newTask = new Task({
				name: request.body.name
			});
			 //save the new task to the database
			newTask.save().then((savedTask,error) =>{
				if(error){
					return {
						message: error.message
					}
				}
				return response.send(201, 'New task created');
			})
		}
	})
})

//[SECTION] Getting all task

app.get('/tasks',(request,response) => {
	Task.find({}).then((result,error) =>{
		if(error){
			return response.send({
				message: error.message 
			})
		}
		return response.status(200).json({
			tasks: result
		})
	})
})


//server Listening
app.listen(port,() => console.log(`server is running at port ${port}`));

module.exports = app;


