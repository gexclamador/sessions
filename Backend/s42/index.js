//server variables for initialization
const express = require('express'); 
const mongoose = require('mongoose'); 
require('dotenv').config()
const taskRoutes = require('./routes/taskRoutes.js');
const app = express();
const port = 4000;

//MongoDB Connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@batch303-exclamador.g2jjkdn.mongodb.net/b303-todo?retryWrites=true&w=majority`,{
	useNewUrlParser: true,
	useUnifiedTopology: true

});

let database = mongoose.connection;

database.on('error', () => console.log('Connection error :('));
database.once('open', () => console.log('Connected to MongoDB!'));

//Middleware Connection
app.use(express.json()); 
app.use(express.urlencoded({extended: true}));
app.use('/api/tasks',taskRoutes); //Initializing the routes for '/tasks so that the server will know the rotes available to send requests to.'

//[SECTION] Mongoose Schema


//[SECTION] Modules



//server Listening
app.listen(port,() => console.log(`server is running at port ${port}`));

module.exports = app;


