const express = require('express');
const router = express.Router(); //the one to establish our route
const TaskController = require('../controllers/TaskController.js');

//Insert routes here

//creating a new task
router.post('/', (request,response) =>{
TaskController.createTask(request.body).then(result => {
		response.send(result); 
	})
})

//[SECTION] Getting all task
 router.get('/',(request,response)=>{
 	TaskController.getAllTasks().then(result => {
 		response.send(result);
 	})
 })
module.exports = router;