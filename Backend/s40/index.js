//server variables for initialization
const express = require('express'); // import express
const app = express(); //initializes express
const port = 4000;

//Middleware
app.use(express.json()); //registering middleware that will make express be able to read JSON format from requests
app.use(express.urlencoded({extended: true})); //middleware that will allow express to be able to read data types other than the defaukt string and array that it can usually read

//server Listening
app.listen(port,() => console.log(`server is running at port ${port}`));


//[SECTION] Routes

app.get('/',(request,response) => {
	response.send('Hello World!');
})
 
 app.post('/greeting',(request,response) => {
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}`);
})

 //REGISTERING USERS
  let users = [];
 app.post('/register',(request,response) => {
 	if(request.body.username !== '' && request.body.password !== ''){
 		users.push(request.body);
 		response.send(`user ${request.body.username} has successfully been registerd!`);
 	}else {
 		response.send('Please input BOTH username AND password.');
 	}
 })

//GET THE LIST/ARRAY OF USERS
 		app.get('/users',(request,response) =>{
 		response.send(users);
 })


module.exports = app;