// console.log(fetch('https://jsonplaceholder.typicode.com/posts'))
// //the fetch() function returns a promise which can then be chained using the then() function.The then function waits for the promise to be resolved before executing.

// fetch('https://jsonplaceholder.typicode.com/posts')
// 	.then(response => response.json())
// 	.then (posts => console.log(posts));



// 	async function fetchData(){
// 		let  result = await  fetch ('https://jsonplaceholder.typicode.com/posts')

// 		let json_result = await result.json();
// 		console.log(json_result);
// 	}

	
// 	fetchData();

// 	//Adding headers,body ,and method to fetch() function

// 	//CREATING NEW POSTS
// 	fetch('https://jsonplaceholder.typicode.com/posts',{
// 	method: 'POST',
// 	headers:{
// 		'Content-Type': 'application/json'
// 	},
// 	body: JSON.stringify({
// 		title: "New post!",
// 		body: "Hello World.",
// 		userId: 2
// 	})
// })

// 	.then(response => response.json())
// 	.then (created_post => console.log(created_post));

// //UPDATING EXISTING POST

// 	// /1 signifies the id of the post you want to update

// 	fetch('https://jsonplaceholder.typicode.com/posts/1',{
// 	method: 'PUT',
// 	headers:{
// 		'Content-Type': 'application/json'
// 	},
// 	body: JSON.stringify({
// 		title: "Corrected Post!"
		
// 	})
// })

// 	.then(response => response.json())
// 	.then (updated_post => console.log(updated_post));


// 	//DELETING EXISTING POST

// 	fetch ('https://jsonplaceholder.typicode.com/posts/1',{
// 		method: 'DELETE'
// 	})
// 	.then(response => response.json())
// 	.then(deleted_post => console.log(deleted_post));

// 	//FILTERING POST
// 	fetch ('https://jsonplaceholder.typicode.com/posts?userId=1')
// 	.then(response => response.json())
// 	.then(post => console.log(post));
// //GETTTING COMMENTSBOF POST
// fetch ('https://jsonplaceholder.typicode.com/posts/1/comments')
// 	.then(response => response.json())
// // 	.then(comments => console.log(comments));






	

	async function getSpecificToDo(){
   
   return await (

      fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'GET'

	})

	.then(response => response.json())
	.then (data => {

		return data})


   )};