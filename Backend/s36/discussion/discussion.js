//mongoDB AGGREGATION

//$match-used to match or get documents that bsatisfies the condition
// $match is similar to find().You can use query operators to make your criteria more flexible
// $match ->Apple,kiwi,banana
db.fruits.aggregate([
  {$match:{onSale:true}},
  // $group:allows us to group together documents and create an analysis  outb of the group elements
  
  /*apple id =1.0
  kiwi = 1.0
  */
  {$group:{_id:"$supplier_id",total:{$sum:"$stock"}}}
  // $sum -used to add or total the values in a given field
]);




db.fruits.aggregate([
  {$match:{onSale:true}},
  {$group:{_id:"$supplier_id",avgStocks:{$avg:"$stock"}}},
{$project:{_id:0}}
]);





db.fruits.aggregate([
  {$match:{onSale:true}},
  {$group:{_id:"$supplier_id",maxPrice:{$max:"$price"}}},
{$sort:{maxPrice:-1}}
]);



db.fruits.aggregate([
  {$unwind:"$origin"}
]);


db.fruits.aggregate([
  {$unwind:"$origin"},
{$group:{_id:"$origin",kinds:{$sum:1}}}
]);