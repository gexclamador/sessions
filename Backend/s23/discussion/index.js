// IF-ELSE STATEMENT
let number = 1;

if (number >= 1 ){
	console.log("The number is greater than 1!");
}else if ( number < 1 ) {
	console.log("The number is less than !");
} else {
	console.log("None of the condtions were true");
}


// Falsely value

if(false){
	console.log("Falsey");
}

if(0){
	console.log("Falsey");
}

if(undefined){
	console.log("Falsey");
}

if(""){
	console.log("Falsey");
}


//TRUTHY

if(true){
	console.log("Truthy");
}

if(1){
	console.log("Truthy");
}
if([]){
	console.log("Truthy");
}

// TERNARY

let result =(1 < 10)? true:false;
  
console.log("Value returned " + result);

// if there are multiple lines within the if-else block,it's better to use the regular if-else syntax as the ternary operator is only capable of handling one-liner

if( 5== 5){
	let greeting= "hello";
	console.log(greeting);
}

// Switch STATEMENT

let day = prompt("What day of the week is today?").toLowerCase();

switch (day) {
	case 'monday' :
		console.log("The day is Moday!");
		break;
	case 'tuesday' :
		console.log("The day is Tuesday!");
		break;
	case 'wednesday' :
		console.log("The day is Wednesday!");
		break;
	case 'whursday' :
		console.log("The day is Thursday!");
		break;

	case 'friday' :
		console.log("The day is Friday!");
		break;


	default:
		console.log("Please enter a valid day naman paareh!");
		break;

}


//TRY/catch/finally
 function showIntensityAlert(windSpeed){
 	try{s
 		alertat(determineTyphoonIntensity(windSpeed));
 	}catch(error){
 		console.log(error.message)

 	}finally {
 		alert("Intensity updates will show new alert");
 	}
 }

 showIntensityAlert(56);



