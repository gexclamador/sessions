console.log("ES6 Updates");
//Exponent Operator
const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow (8, 2);
console.log(secondNum);

//Template Literals

// -allows us to write strings w/o using concatenation
// -greatly helps us with code readability

let name ="ken";




let  message = "hello " + name + " welcome to programming "; 

console.log("Messsage without template literals: " + message);


//using template literls
// backticks (``) and ${} use for including javascript expressions
 message = `hello ${name}! Welcome to Programming`;
console.log(`Messsage with template literals: ${message} `);

//Creates multiline 

const anotherMessage = `
${name} attended a Math competition.
 He Won it by solving the problem 8 ** 2 with the answer of ${firstNum}`;

console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings amount is : ${principal * interestRate }`);

//ARRAY Destructuring

const fullname = ["Juan", "Dela", "Cruz"];

//using array indeces
console.log(fullname[0]);
console.log(fullname[1]);
console.log(fullname[2]);

console.log(`Hello ${fullname[0]} ${fullname[1]} ${fullname[2]}! It's nice to meet you.`);

//Using Array Destructuring

const [firstName ,middleName , lastName] = fullname;
console.log (`hello ${firstName} ${middleName} ${lastName}! it's nice to meet you!`) ;

//Object Destructuring
const person = {
	givenName : 'Jane',
	maidenName: 'Dela',
	familyName: 'Cruz'
}
//using dot notation

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log (`hello ${person.givenName} ${person.maidenName} ${person.familyName}! it's good to see you!`) ;

//Using object destructuring

const {givenName,familyName,maidenName} = person;
console.log (`hello ${givenName} ${maidenName} ${familyName}! it's nice to meet you!`) ;

//Using Object destructuring in functions
function getFullname({givenName,maidenName,familyName}){
	console.log( `${givenName} ${maidenName} ${familyName}`)
};
getFullname(person);

//Arrow Function

const hello = () => {
	console.log('Hello World');
}
hello();

//Traditional functions without template literal
// function printFullName(fName,mName,LName)
// {
// 	console.log( fName + ' ' + mName + ' ' + LName);
// }
// printFullName('John','D','Smith')

// Arrow function with template literals
const printFullName = (fName ,mName ,LName) => {
	console.log (`${fName} ${mName} ${LName}`)
}

printFullName('Jane','D', 'Smith');

//Arow function with loops
const students = ['John' ,'Jane','Judy'];

//traditional function

students.forEach(function(student){
	console.log(` ${student} is a student`)
})
//Arrow function
students.forEach((student) => {
	console.log(` ${student} is a student`)
})

//Implicit Return Statements
//Traditional Function
// function add(x,y){
// 	return x + y;
// }

// let total = add(2,5);
// console.log(total);

//Arrow function 
//works on single line arrow function
 const add = (x,y) => x + y;

 let total = add(2,5);
 console.log(total);

 //DEFAULT ARGUMENTS VALUE

 const greet = (name = 'user') => {
 	return `Good Afternoon ${name}`
 	 }
 	 console.log(greet()); //Good Afternoon user
 	 console.log(greet('Judy')); //Good Afternoon Judy

 	 //[SECTION] Class-based Object Blueprints
 	 //Create a class
 	 class Car {
 	 	constructor(brand, name,year){
 	 		this.brand = brand;
 	 		this.name = name;
 	 		this.year = year;
 	 	}
 	 }
 	 //Instantiate an Object
const fordExplorer = new Car();

//Even though the 'fordExplorer' object is const, cince it is an object, you may still re assign values to its properties
fordExplorer.brand ="Ford";
fordExplorer.name ="Explorer";
fordExplorer.year =2022;

console.log(fordExplorer);

//This logic applies wether you reassign the values of each property separately or put it as argument of the new instance of the class

const toyotaVios = new Car ("Toyota","Vios",2018 );
console.log(toyotaVios);