// alert("Hello World!");
console.log ("hello World!");
// [SECTION] variables


// variable dclaration and invocation
let my_variable = "Hola, Mundo";
console.log(my_variable);

// concatenating strings
let country = "Philippines";
let province =  "Metro Manila";
let full_address =  province + "," + country ;

console.log (full_address);

// NUMBERS/INTEGERS
let headcount = 26;
let grade =98.7;

console.log("The number of student is " + headcount + " and the average grade of all students is " + grade);
let sum = headcount + grade;
console.log(sum);

// Boolean

let is_Married = false;
let is_Good_Conduct = true;
console.log("he's married:" + is_Married);
console.log("she's a good person:" + is_Good_Conduct);

let grades = [98.7 ,89.9 ,90.2 ,94.6]
let details =[ "John" , "Smith" , 32 , true]
console.log(details);

// Objects
let person = {
	fullName: "Juan Dela Cruz",
	age:40,
	contact: [ "0999999","0912345678"],
	address: {houseNumber: "345",
	city:"England"}
}

console.log( typeof person);
console.log( typeof grades);

// Null & undefined

let girlfriend = null;
let full_name;
console.log(girlfriend);
console.log(full_name);


// operators


// Assignment operators
let assignment_number = 0; //initialize zero as the assignment number
assignment_number = assignment_number + 2;
console.log("Result of addition assignment operator: "+ assignment_number);

assignment_number += 2;
console.log ("Result of shorthand addition assignment operator: "+ assignment_number);