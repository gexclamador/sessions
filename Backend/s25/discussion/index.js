// [SECTION] Objects
// -a data type that is used to represents real-world objects
// CREATING OBJECTS USING INITIALIZING OR OBJECT LITERALS

		let cellphone = {
			name: "Nokia 3210",
			manufacturer : 1999
		}
		console.log("Result from creating objects using initializers/ objects literals");
		console.log(cellphone);
		console.log(typeof cellphone);

		//creating objects using a constructor function

		function Laptop(name,manufactureDate){
			this.name = name;
			this.manufactureDate = manufactureDate;

		}

		let laptop = new Laptop("Lenovo" ,2008);
		console.log('Result from creating objects using constructor function')
		console.log(Laptop);

		let laptop2 = new Laptop("Macbook Air",2020);


		console.log('Result from creating objects using constructor function')
		console.log(Laptop);

// [SECTION]Accesing Objects

		//using bracket notation
		console.log("Result from square bracket notation: "+ laptop2["name"]);

		// Using dor notaion
		console.log("Result from square dot notation: "+ laptop2.name);

		//Access array objects

		let array = [laptop,laptop2]
		console.log(array[0]["name"]);
		console.log(array[0].name);


// [SECTION]Adding/Deleting /Reassigning Objects Properties
		//empty Objects
		let car = {};
		//empty object using constructor function
		let myCar = new Object();
		//Adding objects properties

		car.name = "Honda Civic";
		console.log("Result from adding properties using dot notation:");
		console.log(car);

		//adding properties using square brackets

		car["manufacturing date"] = 2019;

		//we cannot access the objects property using dot notation if it has spaces
		console.log(car["manufacturing date"]);
		console.log(car["manufacturing date"]);
		// console.log(car.manufacturing date);


		console.log("Result from adding properties using the square bracket notation: ")
		console.log(car);

		//deleting Object properties

		delete car["manufacturing date"];
		console.log( "result from deleting object properties:")
		console.log(car);


//[SECTION] reassigning object properties
		car.name = "Honda Civic Type R";
		console.log( "result from reassing object properties:")
		console.log(car);

//[SECTION] object methods
// a method is a function which acts as aproperty of an object

		let person = {
			name:"Barbie",
			greet: function(){
				console.log("Hello! My name is " + this.name);
			} 
		}

		console.log(person);
		console.log("This is the result from object methods:");
		person.greet();



		//Adding methods to objects

		person.walk = function(){
			console.log(this.name + " walked 25 steps forward");
		}
		person.walk();

		let friend = {
			firstname: "ken",
			address:{
				city: "Austin",
				state: "Texas",
				country:"USA",
		},
			email: ["ken@gmail.com","ken@mail.com"],
			introduce: function (person){
				console.log("nice to meet you "+ person.name + " I am " + this.firstname +" from " + this.address.state);
			}
		}

		friend.introduce(person);

