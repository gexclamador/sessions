// use 'require' directive to load the Node.js module /create server
// a module is a software component or part of a program that contains one or more routines
// http module lets Node.js transfer data using HYPER TEXT TRANSFER PROTOCOL
// http is a protocol that allows the fetching of resources  like html documents
// createServer() method creates an HTTP server that listens to requests on specific port and gives response back to the client
// 4000 is the port where the server will listen to
// The arguments passed in the function are request and response objects (data type) that contains methods
// The http module has a createServer() method that accepts a function as an argument and allows for a creation of a server
// 200-status code for the response -means OK
// sets the content-type of the response to be a plain text message
 that allow us to receive requests from the client and send responses back to it
 // 200-status code for the response -means OK
    // sets the content-type of the response to be a plain text message
  //send the response with text content 'hello world'
let http = require("http");

http.createServer(function(request,response){

 response.writeHead(200,{'Content-Type': 'text/plain'});

 response.end('Hello World');
}).listen(4000)

console.log('Server is running at port 4000')