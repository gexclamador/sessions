// FUNCTION invocation

function printName(){
	console.log("My name is Jeff");
} //declaring function

	printName();  //invoking function




	// using function by declaring a variable

	let variable_function = function(){
		console.log ("Hello from function expression!")
	}
	variable_function();

	// SCOPING

	let global_variable = "Call me mr. Worldwide";
	console.log(global_variable);

	function showNames(){
		let function_variable= "joe";
		const function_const= "john";

		console.log(function_variable);
		console.log(function_const);
		console.log(global_variable); // this is a global variable since it is declared outside of the function scope and can be called inside and out of a function
	}
    // console.log(function_variable); hindi gagana since it is locally stored inside a function and cant be called outside of the function
	showNames();

	//NESTED FUNCTIONS
	function parentFunction(){
		let name ="Jane"
		function childFunction(){
			let nested_name = "john"
			console.log(name); //gagana sinse nasa loob parin ng parent function
		}
		childFunction();

	}
	parentFunction();



	function printWelcomeMessageForUser(){
		let first_name = prompt("Enter your first name: ");
		let last_name = prompt("Enter your last name: ");
		console.log("Hello, " + first_name	 + "" + last_name+ "!");
		console.log("Welcome sa page ko!");
	}

	printWelcomeMessageForUser();

	//return STATEMENT

	function fullName(){
		return" Glaze E.";
	}

	console.log(fullName());

	