// // insertOne

db.users.insertOne({
	{
      firstName:"Jane",
      lastName : "Doe",
      age:21,
      contact :{
      	phone: "0912345678",
      	email: "janedoe@gmail.com"
      },
	courses: ["Javascript","CSS","Phyton"],
    department : "none"
	
	})

// insert multiple documents at once

db.users.insertMany([
	{
      "firstName":"John",
      "lastName" : "Doe"
	},
	{
		"firstName":"Joseph",
      "lastName" : "Doe"
	}
	]);

//[SECTION] retrieving documents
//retrieving all the inserted users
db.users.find();

//retrieving document from the collection
db.users.find({"firstName" : "John" });

//[SECTION]updating existing documents
//retrieving user using id
db.users.updateOne(
    {
          "_id": ObjectId("64c1c45f98a5baa752634263")

    },
    //second part -use the $set keyword to set specific property of thatb user to a new value
    {
        $set: {
            "lastName": "Gaza"
        }
    }
);


//for updating multipke documents
//update Many allows for modification of 2 or more 
db.users.updateMany(
{
	"lastName": "Doe"
},
{
	$set: {
		"firstName" : "Mary"
	}
}

	);
//[SECTION]deleting documents from a collection
//deleting multiple documents

db.users.deleteMany({"lastName":"Doe"});

//deleting single documents
db.users.deleteOne({
    "_id":ObjectId("64c1cd681f9e8c3de67bca8a")
});

db.rooms.deleteOne({
    "_id": ObjectId("64c1ddb0ad4c3c74d86742fd")

});


