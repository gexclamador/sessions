//JSON Example

// {
// "city" : "Pateros",
// "province": "Metro Manila",
// "country" :"Philippines"
// }

//[SECTION] JSON Arrays

// "cities": [
// {
// 	"city": "Quezon City",
// 	"Province" : "Metro Manila",
// 		"country" :"Philippines"

// },
// {
// 	"city": "Batangas City",
// 	"Province" : "Batangas",
// 	"country" :"Philippines"

// },
// {
// 	"city": "Star City",
// 	"Province" : "Pasay",
// 	"country" :"Philippines"
// 	"rides": [
// 			{
// 				"name": "Star Flyer"
// 			},
// 			{
// 				"name": "Gabi ng lagim"
// 			}

// 			]

// }
// 	]


//[SECTION] JSON METHODS

let zuitt_batches =[
		{ batchName:"303" },
		{ batchName:"271" }
	]
//before json.stringify, javascript reads the variable as a regular JS array
console.log(`Output before stringification: `);
console.log(zuitt_batches);


//after the json.stringify function, javascript now reads the variable as a string (equivalent to converting array into JSON)
console.log(`Output after stringification: `);
console.log(JSON.stringify(zuitt_batches));


//user details
 let first_name = prompt("What is your firstname?");
 let last_name = prompt("What is your lastname?");
//JSON.stringify converts JS array/object into json
 let other_data = JSON.stringify({
 	firstName : first_name,
 	lastName : last_name
 })
 console.log(other_data);

 //[SECTION] convert Stringify JSON into Javascript objects

 let = other_data_JSON = `[{"firstName":"EARL" ,"lastName":"Diaz"}]`;

 //parse function method converts the JSON string into a JS Object/Array
 let parsed_other_data = JSON.parse(other_data_JSON);

 console.log(parsed_other_data);

 console.log(parsed_other_data[0].firstName);




