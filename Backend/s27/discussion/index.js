//continuation of iteration method

//Iteration methods -loops through all the lements to perform repetitive task on the array

//forEach()-to loop through the array
//map()-loops through the array and 
//filter()- returns a new array containing elements which meets the given condition

//Every -it checks if all element meets the given condition
//it will return true if all elemnts meet the given condition ,however,false it it does not meet all the conditions

let numbers = [1,2,3,4,5,6];
let allValid = numbers.every(function(number){
	return number > 3;

})

console.log("result of every() method :")
console.log(allValid);

//result here is False


//SOME -check if atleast one element meest the given condition
//returns true if atleast one element meets the given condition, return false otherwise.
let someValid = numbers.some(function(number){
		return number < 2;
})

console.log("result of some() method :")
console.log(someValid);

//includes() -method that can be "chained" using them one after another 


let products = ["Mouse","keyboard","Laptop","Monitor"];

let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes("a");
})

console.log(filteredProducts);

//reduce()
let iteration = 0;
let reducedArray = numbers.reduce(function(x,y){
	console.warn("current iteration: "+ ++iteration);
	console.log("accumulator: " + x);
	console.log("currentValue: " + y);

	return x + y ;
})
console.log("result of reduce method :"  + reducedArray	);


let productsReduce = products.reduce(function(x,y){
	return x + "" + y 
})
console.log("result of reduce() method:" + productsReduce);