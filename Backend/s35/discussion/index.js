//Greater than operator
db.users.find({
	age:{
		$gt :82
	}
})
//greater than or equal
db.users.find({
	age:{
		$gte :82
	}
})


//Less than operator

db.users.find({
	age:{
		$lt :80
	}
})

//Less than or equal operator

db.users.find({
	age:{
		$lte :82
	}
})

//Regex operator
db.users.find({
	firstName:{
		$regex :'S'
	}
})

//regex with option operator

db.users.find({
	firstName:{
		$regex :'s' ,
		$options:'i'
	}
})


db.users.find({
	lastName:{
		$regex :'T' ,
		$options:'i'
	}
})

//Combining operators

db.users.find({
	age:{
		$gt: 70
	},
		{
		$regex :'g' 
	}
})



db.users.find({
	age:{
		$lte:76
	},
	{
		$regex: 'j',
		$options:'i'
	}
})


//field projection
db.users.find({},
{
	"_id":0
})

db.users.find({},
{
	"firstName":1
})



db.users.find({},
{
	"_id":0,
	firstName: 1
})

