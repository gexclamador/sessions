
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import CourseView from './pages/CourseView';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import Profile from './pages/Profile';
import AddCourse from './pages/AddCourse'; 
import {useState,useEffect} from 'react';  
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import {UserProvider} from './UserContext';
import './App.css';

function App() {
   const[user,setUser] =useState ({
    id: null,
    isAdmin: null
   })

//function for clearing localStorage upon log out
    const unsetUser = () =>{
      localStorage.clear();
    }

    useEffect(() => {
      console.log(user);
      console.log(localStorage);
    },[user])



      useEffect(()=> {
        fetch('http://localhost:4000/users/details',{
          headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        })
        .then (res =>res.json())
        .then(data => {
          console.log(data);

          if (typeof data._id !== "undefined") {
            setUser({
              id: data._id,
              isAdmin: data.isAdmin
            });
          }
        })
      })




  return ( 

    <UserProvider value={{user,setUser,unsetUser}}>  
     <Router>
      <AppNavbar />
      <Container>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/courses' element={<Courses />} /> 
          <Route path='/courses/:courseId' element={<CourseView />} /> 
          <Route path='/addCourse' element={<AddCourse />} />
          <Route path='/register' element={<Register />} />
          <Route path='/login' element={<Login />} />
          <Route path='/logout' element={<Logout />} />
          <Route path='*' element={<Error />} />
           <Route path='/profile' element={<Profile/>}/>
        </Routes>
      </Container>
    </Router>
    </UserProvider>
   
  );
}

export default App;
