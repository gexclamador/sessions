import React, { useState, useEffect } from 'react';
import { Row } from 'react-bootstrap';
import CourseCard from './CourseCard'; 
import CourseSearch from './CourseSearch';
import SearchCoursesByPrice from './SearchCoursesByPrice';

const UserView = ({ coursesData }) => {
  const [activeCourses, setActiveCourses] = useState([]);

  useEffect(() => {
    const activeCoursesData = coursesData.filter(course => course.isActive);
    const activeCoursesElements = activeCoursesData.map(course => (
      <CourseCard course={course} key={course._id} /> // Use the CourseCard component
    ));

    setActiveCourses(activeCoursesElements);
  }, [coursesData]);

  return (
    <div>
      <h2 className="text-center">Courses</h2>
      <Row className="justify-content-center">
      <CourseSearch/>
      <SearchCoursesByPrice/>
        {activeCourses}
      </Row>
    </div>
  );
};

export default UserView;


