import React, { useState } from 'react';


const CourseSearch = () => {
  
  const [minPrice, setMinPrice] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  const [searchResults, setSearchResults] = useState([]);


  const handleSearchByPrice = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/courses/searchByPrice`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ minPrice, maxPrice })
      });
      const data = await response.json();
      setSearchResults(data.courses);
    } catch (error) {
      console.error('Error searching for courses by price:', error);
    }
  };

  return (
    <div>
     
      <h2>Search Courses by Price Range</h2>
      <div className="form-group">
        <label htmlFor="minPrice">Minimum Price:</label>
        <input
          type="number"
          id="minPrice"
          className="form-control"
          value={minPrice}
          onChange={event => setMinPrice(event.target.value)}
        />
      </div>
      <div className="form-group">
        <label htmlFor="maxPrice">Maximum Price:</label>
        <input
          type="number"
          id="maxPrice"
          className="form-control"
          value={maxPrice}
          onChange={event => setMaxPrice(event.target.value)}
        />
      </div>
      <button className="btn btn-primary" onClick={handleSearchByPrice}>
        Search by Price Range
      </button>
      <h3>Search Results:</h3>
      <ul>
        {searchResults.map(course => (
          <li key={course._id}>{course.name} - ${course.price}</li>
        ))}
      </ul>
    </div>
  );
};

export default CourseSearch;

