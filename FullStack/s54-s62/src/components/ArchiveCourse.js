

import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveCourse({ courseId, isActive, fetchData }) {
  const archiveToggle = () => {
    fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/archive`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(response => response.json())
      .then(data => {
        if (data === true) {
          Swal.fire({
            title: 'Success',
            icon: 'success',
            text: 'Course archived successfully!'
          });
          fetchData(); // Refresh courses data after archiving
        } else {
          Swal.fire({
            title: 'Error',
            icon: 'error',
            text: 'An error occurred. Please try again.'
          });
        }
      })
     
  };

  const activateToggle = () => {
    fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/activate`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(response => response.json())
      .then(data => {
        if (data === true) {
          Swal.fire({
            title: 'Success',
            icon: 'success',
            text: 'Course activated successfully!'
          });
          fetchData(); // Refresh courses
    } else {
          Swal.fire({
            title: 'Error',
            icon: 'error',
            text: 'An error occurred. Please try again.'
          });
        }
      })
    
  };

  return (
    <>
      {isActive ? (
        <Button variant="danger" size="sm" onClick={archiveToggle}>
          Archive
        </Button>
      ) : (
        <Button variant="success" size="sm" onClick={activateToggle}>
          Activate
        </Button>
      )}
    </>
  );
}















































// import React from 'react';
// import { Button } from 'react-bootstrap';
// import Swal from 'sweetalert2';

// export default function ArchiveCourse({ courseId, isActive, fetchData }) {
//   const handleAction = () => {
//     const action = isActive ? 'archive' : 'activate';

//     fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/${action}`, {
//       method: 'PUT',
//       headers: {
//         'Content-Type': 'application/json',
//         Authorization: `Bearer ${localStorage.getItem('token')}`
//       }
//     })
//       .then(response => response.json())
//       .then(data => {
//         if (data === true) {
//           Swal.fire({
//             title: 'Success',
//             icon: 'success',
//             text: `Course ${isActive ? 'archived' : 'activated'} successfully!`
//           });
//           fetchData(); // Refresh courses data after archive/activate
//         } else {
//           Swal.fire({
//             title: 'Error',
//             icon: 'error',
//             text: 'An error occurred. Please try again.'
//           });
//         }
//       })
//       .catch(error => {
//         console.error('Error:', error);
//         Swal.fire({
//           title: 'Error',
//           icon: 'error',
//           text: 'An error occurred. Please try again.'
//         });
//       });
//   };

//   return (
//     <Button variant={isActive ? 'danger' : 'success'} size="sm" onClick={handleAction}>
//       {isActive ? 'Archive' : 'Activate'}
//     </Button>
//   );
// }
