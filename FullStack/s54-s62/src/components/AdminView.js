import React, { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';
import EditCourse from './EditCourse';
import ArchiveCourse from './ArchiveCourse';

const AdminView = ({ coursesData, fetchData }) => {
  const [coursesTable, setCoursesTable] = useState(null);

  useEffect(() => {
    const coursesElements = coursesData.map(course => (
      <tr key={course._id}>
        <td>{course._id}</td>
        <td>{course.name}</td>
        <td>{course.description}</td>
        <td>PHP {course.price}</td>
        <td>
          <span className={`text-${course.isActive ? 'success' : 'danger'}`}>
            {course.isActive ? 'Available' : 'Unavailable'}
          </span>
        </td>
        <td>
          <EditCourse course={course._id} fetchData={fetchData} />
        </td>
        <td>
          <ArchiveCourse courseId={course._id} isActive={course.isActive} fetchData={fetchData} />
        </td>
      </tr>
    ));

    setCoursesTable(coursesElements);
  }, [coursesData, fetchData]);

  return (
    <div>
      <h2 className="text-center">Admin Dashboard</h2>
      <Table striped bordered hover responsive>
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Availability</th>
            <th colSpan="2">Actions</th>
          </tr>
        </thead>
        <tbody>{coursesTable}</tbody>
      </Table>
    </div>
  );
};

export default AdminView;
