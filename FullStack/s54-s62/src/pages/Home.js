import Banner from '../components/Banner.js';
import FeaturedCourses from '../components/FeaturedCourses.js';
import Highlights from '../components/Highlights.js';

// import CourseCard from '../components/CourseCard.js';

export default function Home(){
	return(
		<>
		<Banner/>
		<FeaturedCourses/>
		<Highlights/>
		
		</>

		)
}
