
import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';

export default function Courses() {
  const { user } = useContext(UserContext);
  const [coursesData, setCoursesData] = useState([]);




  const fetchData =()=>{
    fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
      .then(res => res.json())
      .then(data => {
        setCoursesData(data);
      })
  };


 useEffect(() =>{
     fetchData();
},[]);


  return (
    <div>
      {user.isAdmin ? (
       <AdminView coursesData={coursesData} fetchData={fetchData} />
      ) 

      :

       (
        <UserView coursesData={coursesData} />
      )}
    </div>
  );
}











































// //OLD SYNTAX


// // import coursesData from '../data/coursesData.js';
// import {useEffect, useState} from 'react';
// import CourseCard from '../components/CourseCard.js';

// export default	function Courses(){
// 	//state that will be used to store courses from the database
// 		const [courses,setCourses]=useState([]);

// 		useEffect(()=>{
// 			fetch(`${process.env.REACT_APP_API_URL}/courses/`)
// 			.then(res => res.json())
// 			.then(data => {
// 				console.log(data);

// 		setCourses(data.map(course_item =>{
// 				return(
// 					<CourseCard key={course_item.id} course={course_item}/>

// 					)
// 				}))
// 			})
// 		},[]);

// //old approach:
// 	// const courses = coursesData.map(course_item =>{
// 	// 	return(
// 	// 		<CourseCard key={course_item.id} course={course_item}/>

// 	// 		)
// 	// })

// 	return(
// 		<>
//  		 <h1>Courses</h1>
//  				{courses}
//  			</>
// 		)
// } 