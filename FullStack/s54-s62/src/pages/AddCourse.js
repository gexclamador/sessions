import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function AddCourse() {
    const { user } = useContext(UserContext);
    const navigate = useNavigate();

    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);
    const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        if (name !== '' && description !== '' && price > 0) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [name, description, price]);

    function addCourse(event) {
        event.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/courses/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        }) 
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if (data) {
                    Swal.fire({
                        title: 'Course Added',
                        icon: 'success',
                        text: 'You have successfully added a new course.'
                    });


                // Reset input fields
                setName('');
                setDescription('');
                setPrice(0);

                    // Redirect the user to the Courses page after successful course creation.
                    // You should replace "/courses" with your actual route path for the Courses page.
                    navigate('/courses');
                } else {
                    Swal.fire({
                        title: 'Unsuccessful Course Creation',
                        icon: 'error',
                        text: 'Course creation failed. Please try again.'
                    });
                }

            });
    }

    return (
        <>
            {user.isAdmin ? (
                <Form onSubmit={addCourse}>
                    <h1 className="my-5 text-center">Add Course</h1>
                    <Form.Group>
                        <Form.Label>Name:</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter Course Name"
                            required
                            value={name}
                            onChange={e => setName(e.target.value)}
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Description:</Form.Label>
                        <Form.Control
                            as="textarea"
                            rows={3}
                            placeholder="Enter Course Description"
                            required
                            value={description}
                            onChange={e => setDescription(e.target.value)}
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Price:</Form.Label>
                        <Form.Control
                            type="number"
                            placeholder="Enter Course Price"
                            required
                            value={price}
                            onChange={e => setPrice(e.target.value)}
                        />
                    </Form.Group>

                    <Button variant="primary" type="submit" disabled={!isActive}>
                        Add Course
                    </Button>
                </Form>
            ) 
            : (
               navigate('/courses')
            )}
        </>
    );
}
